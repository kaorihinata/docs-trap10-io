---
author:     "NN"
categories: streaming
date:       2022-10-16 19:27:00 -0400
layout:     post
title:      "Royalty Free Audio: Dova-Syndrome"
---

[Dova-Syndrome][dova_s] is a popular Japanese website for providing royalty
free background music (BGM) and sound effects (SE) for both commercial,
and/or non-commercial use. The website serves as both a repository for
existing works, and a communication channel between users and creators for
commissioning new works. In true Japanese fashion, the creator runs the
website [as a hobby][dova_s_en_about], much like the famed Japanese clip-art
site, [いらすとや][irasutoya] (lit. Irasutoya), run by MIFUNE Takashi.

Notable users of the service include:

* Multiple branches of [Cover Corp.][cover_corp]
* [VOMS Project][voms_project]
* [にじさんじ][nijisanji] (lit. Nijisanji)

For western users, an [English language version][dova_s_en] of the website
is available as well.

[cover_corp]:      https://cover-corp.com
[dova_s]:          https://dova-s.jp
[dova_s_en]:       https://dova-s.jp/EN/
[dova_s_en_about]: https://dova-s.jp/EN/_contents/about/
[irasutoya]:       https://www.irasutoya.com
[nijisanji]:       https://www.nijisanji.jp
[voms_project]:    https://voms.net

Caveats for English Speakers
----------------------------

While an [English language version][dova_s_en] of the website is available,
it comes with a few caveats:

1. Without saved search settings, you may end up being repeatedly redirected
   to the Japanese website without explanation.
2. Creators are not required to provide an English translation for their
   profile, or for any of the text accompanying their works. This includes
   the "**Composer's Terms and Conditions of Use materials**" section of the
   profile, which would contain any other conditions for using their works
   (if provided.)

There are a few ways to solve, or at least mitigate all of these issues.

[dova_s_en_license]: https://dova-s.jp/EN/_contents/license/

Caveat 1: Setting UI Language to English
========================================

When first visiting the [English language version][dova_s_en] of the
website, you will have no saved search settings. As a result, visiting any
page that displays search results may result in the interface language being
reverted back to Japanese. The easiest way to avoid this is by setting the
UI language to "English", and saving your search settings. To do this:

1. Navigate to the (misleadingly named)
   "[**Advanced search**][dova_s_en_advanced_search]" page.
2. Under "**UI Language / UI言語**" select "**English / 英語**". If it's
   been selected already, leave it selected, and continue.

   !["The UI Language / UI言語 setting."](
       {{ "/assets/images/ui_language.png" | prepend: site.baseurl }}
       "The UI Language / UI言語 setting."
   ){:width="75%"}
3. Scroll to the bottom of the page, and click the "**Save Settings**"
   button.

   ![The Save Settings button.](
       {{ "/assets/images/save_settings.png" | prepend: site.baseurl }}
       "The Save Settings button."
   ){:width="75%"}

Search results should now display consistently in English, and you should no
longer be redirected to the Japanese website.

[dova_s_en_advanced_search]: https://dova-s.jp/EN/_contents/settingSound/

Caveat 2: Ensuring License Compliance
=====================================

While licensing may sound like an intimidating topic at first, it's not
actually much of an issue once you know where to look, what to look for,
and what to avoid. Starting off, license information is found in 2 primary
locations on Dova-Syndrome:

1. The [Sound Usage License][dova_s_en_license] is the default site license
   by which all works that do not specify other conditions are bound.
2. Optionally, a creator may specify other conditions, which supersede the
   site license. Such conditions will appear in the "**Composer's Terms
   and Conditions of Use materials**" section of the creator's profile.

Of the 2 locations above, the only one you need to be concerned about is the
creator's profile, as the "**Composer's Terms and Conditions of Use
materials**" section is not optional, and in the event that a creator opts
to not specify other conditions, an appropriate message, and a link to the
[Sound Usage License][dova_s_en_license] will be inserted instead. For
example:

![An example of a profile with no extra license conditions.](
    {{ "/assets/images/sound_usage_license.png" | prepend: site.baseurl }}
    "An example of a profile with no extra license conditions."
){:width="75%"}

In the event that you come across a creator that *does* specify other
conditions, don't be discouraged. While the red text on light red may look
like bad news to a non-native speaker, a lot of creators are usually only
asking for small things like attribution, or thanking you for using their
works. For example (from the profile of
[Fukagawa][dova_s_profile_fukagawa]):

![An example of a profile with extra license conditions.](
    {{ "/assets/images/with_conditions.png" | prepend: site.baseurl }}
    "An example of a profile with extra license conditions."
){:width="75%"}

This translates roughly to (courtesy of DeepL):

> If you can give us credit (it's not a must), please do so by putting
> "Fukagawa"\* in the credits.
>
> \* The name has been changed slightly since July 28, 2022. If you have
> given us credit before this date [using the old name], you may leave it as
> it is.
>
> The conditions of use are in accordance with Dova's [Sound Usage License].
>
> We have been active for about a year now, and have received many reports
> of use during that time.
>
> Thank you for your continued support!

If you are able to translate the conditions yourself, then by all means, do
so. If you are not, then I would highly recommend enlisting the use of a
quick translation tool like [DeepL][deepl], or in a pinch,
[Google Translate][google_translate] as the conditions are often written to
be understood easily.

[deepl]: https://www.deepl.com/en/app/
[dova_s_profile_fukagawa]: https://dova-s.jp/EN/_contents/author/profile441.html
[google_translate]: https://translate.google.com

A Cautionary Tale on Licensing
==============================

While Dova-Syndrome makes it very easy to deal with licensing, you may
occasionally run into situations where a creator distributing their work on
Dova-Syndrome attracts the attention of a malicious individual attempting
to game the YouTube copyright strike system to harm creators.

Consider the following [Reddit post][reddit_post_a]. While the individual in
question is most likely incorrect to blame Dova-Syndrome (see:
[Use on YouTube][dova_s_en_use_on_youtube]), the lesson to take away from
the situation is pretty clear: using audio that has gained a lot of
attention due to its association with a wildly popular personality comes
with a certain amount of risk, and YouTube has no incentive to protect small
creators.

The bottom line is, if you want to use the same music as another content
creator, either because you like that music, and/or you like that content
creator, and there is nothing legally saying you can't, then by all means,
do so. However, you also have to accept the risk that comes with that
decision.

[dova_s_en_use_on_youtube]: https://dova-s.jp/EN/_contents/other/youtube.html
[reddit_post_a]: https://www.reddit.com/r/VirtualYoutubers/comments/go3t2r/a_warning_to_fellow_translators_about_hololive/].
